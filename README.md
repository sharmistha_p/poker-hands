# Poker Hand Sorter
### PreRequisite
* Java 1.8
* Maven 3.*
### How to Build
* Checkout repository
* Run `mvn clean build` to create a executable jar
* Executable jar will be created in `./target` with name `poker-1.0-SNAPSHOT.jar`
### How to run tests
* `mvn clean test`
### How to run the application
* Copy `/src/main/resources/poker-hands.txt` into `/target` folder
* run `cat poker-hands.txt | java -jar poker-1.0-SNAPSHOT.jar`

### Output
![img.png](img.png)

### Code coverage
![img_1.png](img_1.png)