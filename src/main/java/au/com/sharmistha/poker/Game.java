package au.com.sharmistha.poker;

import au.com.sharmistha.poker.domain.Card;
import au.com.sharmistha.poker.domain.Player;
import au.com.sharmistha.poker.rule.*;
import au.com.sharmistha.poker.utils.CardUtil;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static au.com.sharmistha.poker.rule.RuleResult.PLAYER1_WON;
import static au.com.sharmistha.poker.rule.RuleResult.PLAYER2_WON;
import static java.lang.String.format;

class GameResult{

    private final int player1Hands;
    private final int player2Hands;

    public GameResult(int player1Hands, int player2Hands) {
        this.player1Hands = player1Hands;
        this.player2Hands = player2Hands;
    }

    public int getPlayer1Hands() {
        return player1Hands;
    }

    public int getPlayer2Hands() {
        return player2Hands;
    }
}

public class Game {

    private static final List<CardChecker> rules = Arrays.asList(
            new RoyalFlush(),
            new StraightFlush(),
            new FourOfAKind(),
            new FullHouse(),
            new Flush(),
            new Straight(),
            new ThreeOfAKind(),
            new TwoPairs(),
            new Pair(),
            new HighCard());


    public GameResult play(InputStream inStream) {
        Player player1 = new Player();
        Player player2 = new Player();

        try (Scanner scanner = new Scanner(inStream)) {
            while (scanner.hasNextLine()) {
                String stringCards = scanner.nextLine();
                List<Card> cardList = CardUtil.prepareCardLists(stringCards);
                player1.issueCards(cardList.subList(0, 5));
                player2.issueCards(cardList.subList(5, 10));
                Round round = new Round(rules, player1.getCards(), player2.getCards());
                RuleResult result = round.play();
                if(result == PLAYER1_WON) player1.incrementHand();
                if(result == PLAYER2_WON) player2.incrementHand();
            }
        }

        printResult(player1, player2);
        return new GameResult(player1.getHands(), player2.getHands());
    }

    private void printResult(Player player1, Player player2) {
        System.out.printf("Player 1: %s hands%n", player1.getHands());
        System.out.printf("Player 2: %s hands%n", player2.getHands());
    }
}
