package au.com.sharmistha.poker;

import au.com.sharmistha.poker.domain.Card;
import au.com.sharmistha.poker.rule.CardChecker;
import au.com.sharmistha.poker.rule.RuleResult;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;

public class Round {

    private final List<CardChecker> rules;
    private final List<Card> player1Cards;
    private final List<Card> player2Cards;

    public Round(List<CardChecker> rules, List<Card> player1Cards, List<Card> player2Cards) {
        this.rules = rules;
        this.player1Cards = player1Cards;
        this.player2Cards = player2Cards;
    }

    public RuleResult play() {
        for (CardChecker rule: rules) {
            RuleResult result = rule.check(player1Cards, player2Cards);
            if(result == PLAYER1_WON || result == PLAYER2_WON) {
                return result;
            }
        }
        return TIE;
    }

}
