package au.com.sharmistha.poker.domain;

public class Card implements Comparable<Card> {

    private final Value value ;
    private final Suit suit;

    public Card(String cardValue) {
        this.value = Value.fromString(cardValue.substring(0,1));
        this.suit = Suit.fromString(cardValue.substring(1));
    }

    public Value getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }

    @Override
    public int compareTo(Card o) {
        return value.getValue().compareTo(o.value.getValue());
    }
}
