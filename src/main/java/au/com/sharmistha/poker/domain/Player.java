package au.com.sharmistha.poker.domain;

import java.util.List;

public class Player {

    private int hands = 0;
    private List<Card> cards;

    public void issueCards(List<Card> cards) {
        this.cards = cards;
    }

    public int getHands() {
        return hands;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void incrementHand() {
        hands++;
    }

}
