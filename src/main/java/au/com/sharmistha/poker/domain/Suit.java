package au.com.sharmistha.poker.domain;

public enum Suit {
    DIAMONDS("D"), HEARTS("H"), SPADES("S"), CLUBS("C");

    private final String suit;

    Suit(String suit) {
        this.suit = suit;
    }

    public String getSuit() {
        return suit;
    }

    public static Suit fromString(String input) {
        for (Suit suitValue : Suit.values()) {
            if (suitValue.suit.equalsIgnoreCase(input)) {
                return suitValue;
            }
        }
        throw new IllegalArgumentException("Unknown card suit");
    }

}
