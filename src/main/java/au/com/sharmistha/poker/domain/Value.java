package au.com.sharmistha.poker.domain;

public enum Value {

    TWO("2", 2), THREE("3", 3), FOUR("4", 4), FIVE("5", 5), SIX("6", 6), SEVEN("7", 7), EIGHT("8", 8), NINE("9", 9), TEN("T", 10), JACK("J", 11), QUEEN("Q", 12), KING("K", 13), ACE("A", 14);

    private final Integer value;
    private final String symbol;

    Value(String symbol, int value) {
        this.value = value;
        this.symbol = symbol;
    }

    public Integer getValue() {
        return value;
    }

    public static Value fromString(String input) {
        for (Value value : Value.values()) {
            if (value.symbol.equalsIgnoreCase(input)) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown card value");
    }
}
