package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

abstract class AbstractOfAKind implements CardChecker {

    protected RuleResult checkOfAKind(List<Card> player1Cards, List<Card> player2Cards, Long occurrences) {
        Map<Integer, Long> player1SameValueCountMap = getSameValueCountMap(player1Cards);
        Map<Integer, Long> player2SameValueCountMap = getSameValueCountMap(player2Cards);

        if(player1SameValueCountMap.containsValue(occurrences) && player2SameValueCountMap.containsValue(occurrences)) {
            Optional<Map.Entry<Integer, Long>> player1 = player1SameValueCountMap.entrySet().stream().filter(entry -> entry.getValue().equals(occurrences)).findFirst();
            Optional<Map.Entry<Integer, Long>> player2 = player2SameValueCountMap.entrySet().stream().filter(entry -> entry.getValue().equals(occurrences)).findFirst();

            if(player1.get().getKey() > player2.get().getKey())  return PLAYER1_WON;
            if(player2.get().getKey() > player1.get().getKey())  return PLAYER2_WON;
        } else {
            if(player1SameValueCountMap.containsValue(occurrences)) return PLAYER1_WON;
            if(player2SameValueCountMap.containsValue(occurrences)) return PLAYER2_WON;
        }
        return TIE;
    }

    protected Map<Integer, Long> getSameValueCountMap(List<Card> cardList) {
        return cardList.stream()
                .collect(groupingBy(card -> card.getValue().getValue(), counting()));
    }

}
