package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;

public interface CardChecker {

     RuleResult check(List<Card> player1Cards, List<Card> player2Cards);

}
