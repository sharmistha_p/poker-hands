package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.isAllCardsWithSameSuit;

public class Flush implements CardChecker{
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        boolean player1WithAllSameSuit = isAllCardsWithSameSuit(player1Cards);
        boolean player2WithAllSameSuit = isAllCardsWithSameSuit(player2Cards);

        if (player1WithAllSameSuit && !player2WithAllSameSuit) return PLAYER1_WON;
        if (player2WithAllSameSuit && !player1WithAllSameSuit) return PLAYER2_WON;
        return TIE;
    }


}
