package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;

import static au.com.sharmistha.poker.utils.Constants.OCCURRENCES_FOUR;

public class FourOfAKind extends AbstractOfAKind {

    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        return super.checkOfAKind(player1Cards, player2Cards, OCCURRENCES_FOUR);
    }
}
