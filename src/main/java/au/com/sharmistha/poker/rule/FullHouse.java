package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.Constants.OCCURRENCES_THREE;
import static au.com.sharmistha.poker.utils.Constants.OCCURRENCES_TWO;

public class FullHouse extends AbstractOfAKind {
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        Optional<Map.Entry<Integer, Long>> player1TwoOfAKind = getSameValueCountMap(player1Cards).entrySet().stream()
                .filter(entry -> entry.getValue().equals(OCCURRENCES_TWO))
                .findFirst();
        Optional<Map.Entry<Integer, Long>> player2TwoOfAKind = getSameValueCountMap(player2Cards).entrySet().stream()
                .filter(entry -> entry.getValue().equals(OCCURRENCES_TWO))
                .findFirst();

        Optional<Map.Entry<Integer, Long>> player1ThreeOfAKind = getSameValueCountMap(player1Cards).entrySet().stream()
                .filter(entry -> entry.getValue().equals(OCCURRENCES_THREE))
                .findFirst();
        Optional<Map.Entry<Integer, Long>> player2ThreeOfAKind = getSameValueCountMap(player2Cards).entrySet().stream()
                .filter(entry -> entry.getValue().equals(OCCURRENCES_THREE))
                .findFirst();

        if(player1TwoOfAKind.isPresent() && player1ThreeOfAKind.isPresent() && player2TwoOfAKind.isPresent() && player2ThreeOfAKind.isPresent()) {
            if(player1ThreeOfAKind.get().getValue() > player2ThreeOfAKind.get().getValue()) return PLAYER1_WON;
            if(player2ThreeOfAKind.get().getValue() > player1ThreeOfAKind.get().getValue()) return PLAYER1_WON;
        } else {
            if(player1TwoOfAKind.isPresent() && player1ThreeOfAKind.isPresent()) return PLAYER1_WON;
            if(player2TwoOfAKind.isPresent() && player2ThreeOfAKind.isPresent()) return PLAYER2_WON;
        }
        return TIE;
    }
}
