package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.Collections;
import java.util.List;


public class HighCard implements CardChecker{
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards){
        player1Cards.sort(Collections.reverseOrder());
        player2Cards.sort(Collections.reverseOrder());

        for (int i = 0; i < player1Cards.size(); i++) {
            if((player1Cards.get(i).compareTo(player2Cards.get(i)) > 0)){
                return RuleResult.PLAYER1_WON;
            } else if((player1Cards.get(i).compareTo(player2Cards.get(i))) < 0){
                return RuleResult.PLAYER2_WON;
            }
        }
        return RuleResult.TIE;
    }
}
