package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.Collections;
import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.isAllCardValuesConsecutive;
import static au.com.sharmistha.poker.utils.CardUtil.isAllCardsWithSameSuit;

public class RoyalFlush implements CardChecker{
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        Collections.sort(player1Cards);
        Collections.sort(player2Cards);
        boolean player1 = isAllCardValuesConsecutive(player1Cards) && player1Cards.get(0).getValue().getValue().equals(10) && isAllCardsWithSameSuit(player1Cards);
        boolean player2 = isAllCardValuesConsecutive(player2Cards) && player2Cards.get(0).getValue().getValue().equals(10) && isAllCardsWithSameSuit(player2Cards);

        if(player1 && !player2) return  PLAYER1_WON;
        if(player2 && !player1) return  PLAYER2_WON;
        return TIE;
    }
}
