package au.com.sharmistha.poker.rule;

public enum RuleResult {
    PLAYER1_WON, PLAYER2_WON, TIE

}
