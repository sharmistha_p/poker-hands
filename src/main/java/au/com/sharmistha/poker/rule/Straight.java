package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.isAllCardValuesConsecutive;

public class Straight implements CardChecker{
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        boolean player1ListIsConsecutive = isAllCardValuesConsecutive(player1Cards);
        boolean player2ListIsConsecutive = isAllCardValuesConsecutive(player2Cards);

        if (player1ListIsConsecutive && !player2ListIsConsecutive) return PLAYER1_WON;
        if (player2ListIsConsecutive && !player1ListIsConsecutive) return PLAYER2_WON;
        return TIE;
    }

}
