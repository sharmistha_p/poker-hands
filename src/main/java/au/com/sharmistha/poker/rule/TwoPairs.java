package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;

import java.util.List;
import java.util.Map;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.Constants.OCCURRENCES_TWO;

public class TwoPairs extends AbstractOfAKind{
    @Override
    public RuleResult check(List<Card> player1Cards, List<Card> player2Cards) {
        Map<Integer, Long> player1SameValueCountMap = super.getSameValueCountMap(player1Cards);
        Map<Integer, Long> player2SameValueCountMap = super.getSameValueCountMap(player2Cards);

        long player1Count = player1SameValueCountMap.values().stream().filter(value -> value.equals(OCCURRENCES_TWO)).count();
        long player2Count = player2SameValueCountMap.values().stream().filter(value -> value.equals(OCCURRENCES_TWO)).count();

        if(player1Count == OCCURRENCES_TWO && player2Count != OCCURRENCES_TWO) return PLAYER1_WON;
        if(player2Count == OCCURRENCES_TWO && player1Count != OCCURRENCES_TWO) return PLAYER2_WON;
        return TIE ;
    }
}
