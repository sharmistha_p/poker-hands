package au.com.sharmistha.poker.utils;

import au.com.sharmistha.poker.domain.Card;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CardUtil {
    private CardUtil(){}

    public static boolean isAllCardValuesConsecutive(List<Card> cardList) {
        Collections.sort(cardList);
        for (int i = 0; i < cardList.size() - 1; i++) {
            if(cardList.get(i).getValue().getValue() + 1 != cardList.get(i+1).getValue().getValue()){
                return false;
            }
        }
        return true;
    }

    public static boolean isAllCardsWithSameSuit(List<Card> cardList){
        String firstSuit = cardList.get(0).getSuit().getSuit();
        return cardList.stream().allMatch( suit-> suit.getSuit().getSuit().equals(firstSuit));
    }

    public static List<Card> prepareCardLists(String cardInput){
        String[] tokens = cardInput.split(" ");
        return Arrays.stream(tokens).map(Card::new).collect(Collectors.toList());
    }
}
