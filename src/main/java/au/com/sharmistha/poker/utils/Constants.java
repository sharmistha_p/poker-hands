package au.com.sharmistha.poker.utils;

public interface Constants {
    Long OCCURRENCES_TWO = 2L;
    Long OCCURRENCES_THREE = 3L;
    Long OCCURRENCES_FOUR = 4L;
}
