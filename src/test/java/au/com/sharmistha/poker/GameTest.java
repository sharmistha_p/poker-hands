package au.com.sharmistha.poker;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

public class GameTest {

    @Test
    public void pairTest() {
        Game game = new Game();
        String input = "4H 4C 6S 7S KD 2C 3S 9S 9D TD";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        GameResult gameResult = game.play(inputStream);
        assertThat(gameResult.getPlayer1Hands()).isEqualTo(0);
        assertThat(gameResult.getPlayer2Hands()).isEqualTo(1);
    }

    @Test
    public void highestCardTest() {
        Game game = new Game();
        String input = "5D 8C 9S JS AC 2C 5C 7D 8S QH";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        GameResult gameResult = game.play(inputStream);
        assertThat(gameResult.getPlayer1Hands()).isEqualTo(1);
        assertThat(gameResult.getPlayer2Hands()).isEqualTo(0);
    }

    @Test
    public void flushTest() {
        Game game = new Game();
        String input = "2D 9C AS AH AC 3D 6D 7D TD QD";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        GameResult gameResult = game.play(inputStream);
        assertThat(gameResult.getPlayer1Hands()).isEqualTo(0);
        assertThat(gameResult.getPlayer2Hands()).isEqualTo(1);
    }

    @Test
    public void pairWithHighestCardTest() {
        Game game = new Game();
        String input = "4D 6S 9H QH QC 3D 6D 7H QD QS";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        GameResult gameResult = game.play(inputStream);
        assertThat(gameResult.getPlayer1Hands()).isEqualTo(1);
        assertThat(gameResult.getPlayer2Hands()).isEqualTo(0);
    }

    @Test
    public void fullHouseWithThreeFoursTest() {
        Game game = new Game();
        String input = "2H 2D 4C 4D 4S 3C 3D 3S 9S 9D";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        GameResult gameResult = game.play(inputStream);
        assertThat(gameResult.getPlayer1Hands()).isEqualTo(1);
        assertThat(gameResult.getPlayer2Hands()).isEqualTo(0);
    }

}