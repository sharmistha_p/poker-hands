package au.com.sharmistha.poker;

import au.com.sharmistha.poker.domain.Card;
import au.com.sharmistha.poker.rule.CardChecker;
import au.com.sharmistha.poker.rule.RuleResult;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class RoundTest {

    @Test
    public void player1WonTest() {
        CardChecker rule = Mockito.mock(CardChecker.class);
        List<Card> player1Cards = prepareCardLists("AH 9S 4D TD 8S");
        List<Card> player2Cards = prepareCardLists("4H JS 3C TC 8D");

        Round round = new Round(Collections.singletonList(rule), player1Cards, player2Cards);
        when(rule.check(player1Cards, player2Cards)).thenReturn(PLAYER1_WON);
        RuleResult result = round.play();
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WonTest() {
        CardChecker rule = Mockito.mock(CardChecker.class);
        List<Card> player1Cards = prepareCardLists("AH 9S 4D TD 8S");
        List<Card> player2Cards = prepareCardLists("4H JS 3C TC 8D");

        Round round = new Round(Collections.singletonList(rule), player1Cards, player2Cards);
        when(rule.check(player1Cards, player2Cards)).thenReturn(PLAYER2_WON);
        RuleResult result = round.play();
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void shouldExecuteNextRuleIfPreviousRuleResultingInTieTest() {
        CardChecker rule1 = Mockito.mock(CardChecker.class);
        CardChecker rule2 = Mockito.mock(CardChecker.class);
        List<CardChecker> rules = Arrays.asList(rule1, rule2);

        List<Card> player1Cards = prepareCardLists("AH 9S 4D TD 8S");
        List<Card> player2Cards = prepareCardLists("4H JS 3C TC 8D");

        Round round = new Round(rules, player1Cards, player2Cards);

        when(rule1.check(player1Cards, player2Cards)).thenReturn(TIE);
        when(rule2.check(player1Cards, player2Cards)).thenReturn(PLAYER1_WON);

        RuleResult result = round.play();
        assertThat(result).isEqualTo(PLAYER1_WON);
        verify(rule1, times(1)).check(player1Cards, player2Cards);
        verify(rule2, times(1)).check(player1Cards, player2Cards);
    }

    @Test
    public void tieTest() {
        CardChecker rule1 = Mockito.mock(CardChecker.class);
        List<CardChecker> rules = Collections.singletonList(rule1);

        List<Card> player1Cards = prepareCardLists("AH 9S 4D TD 8S");
        List<Card> player2Cards = prepareCardLists("4H JS 3C TC 8D");

        Round round = new Round(rules, player1Cards, player2Cards);

        when(rule1.check(player1Cards, player2Cards)).thenReturn(TIE);
        RuleResult result = round.play();
        assertThat(result).isEqualTo(TIE);
    }


}