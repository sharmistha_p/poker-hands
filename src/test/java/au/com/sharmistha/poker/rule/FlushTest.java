package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class FlushTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new Flush();

        List<Card> player1Cards = prepareCardLists("TH 2H 6H KH AH");
        List<Card> player2Cards = prepareCardLists("2C KS 9S AD TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new Flush();

        List<Card> player1Cards = prepareCardLists("TC 2H 6S KH AH");
        List<Card> player2Cards = prepareCardLists("2C KC 9C AC TC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new Flush();

        List<Card> player1Cards = prepareCardLists("TH 2H 6H KH AH");
        List<Card> player2Cards = prepareCardLists("TC 2C 6C KC AC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }

}