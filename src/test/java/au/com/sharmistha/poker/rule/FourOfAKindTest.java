package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class FourOfAKindTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new FourOfAKind();

        List<Card> player1Cards = prepareCardLists("AH AC AS AD KD");
        List<Card> player2Cards = prepareCardLists("2C 9S KC QD 9H");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new FourOfAKind();

        List<Card> player1Cards = prepareCardLists("4H 4C TS 4D KD");
        List<Card> player2Cards = prepareCardLists("2C KS KH KD KC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieWithSameSuitSameValueTest(){
        CardChecker cardChecker = new FourOfAKind();

        List<Card> player1Cards = prepareCardLists("3H 5C 5C 5C 5C");
        List<Card> player2Cards = prepareCardLists("5C 5C 5C 5C 2C");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new FourOfAKind();

        List<Card> player1Cards = prepareCardLists("5H 5S 5D 5C 3H");
        List<Card> player2Cards = prepareCardLists("2C 5H 5C 5S 5D");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }

}