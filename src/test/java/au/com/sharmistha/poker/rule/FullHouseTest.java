package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class FullHouseTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new FullHouse();

        List<Card> player1Cards = prepareCardLists("2H 2S 2C TH TS");
        List<Card> player2Cards = prepareCardLists("2C KS 9S AD TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new FullHouse();

        List<Card> player1Cards = prepareCardLists("2H KS QC TH TS");
        List<Card> player2Cards = prepareCardLists("4C 8S 4S 8D 8C");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new FullHouse();

        List<Card> player1Cards = prepareCardLists("2H 2S 2C TH TS");
        List<Card> player2Cards = prepareCardLists("4C 8S 4S 8D 8C");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}