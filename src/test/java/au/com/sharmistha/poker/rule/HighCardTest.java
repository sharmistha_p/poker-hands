package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;


public class HighCardTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new HighCard();

        List<Card> player1Cards = prepareCardLists("AH QH 3H KH TH");
        List<Card> player2Cards = prepareCardLists("3H 4D 2H 5H 8H");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new HighCard();

        List<Card> player1Cards = prepareCardLists("3H 7H 8H 6H 2H");
        List<Card> player2Cards = prepareCardLists("9S 4D 2H 5H 8H");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new HighCard();

        List<Card> player1Cards = prepareCardLists("AH QH 3H KH TH");
        List<Card> player2Cards = prepareCardLists("3C KC TC AC QC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }

    @Test
    public void tieOnFewCardsTest(){
        CardChecker cardChecker = new HighCard();

        List<Card> player1Cards = prepareCardLists("AH QH 3H KH TH");
        List<Card> player2Cards = prepareCardLists("AC QC 3C 2S 4C");

        RuleResult ruleResult = cardChecker.check(player1Cards, player2Cards);
        assertThat(ruleResult).isEqualTo(PLAYER1_WON);
    }

}