package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class PairTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new Pair();

        List<Card> player1Cards = prepareCardLists("6H 4C 7D 7S KD");
        List<Card> player2Cards = prepareCardLists("TC 3S 9S 2D QD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new Pair();

        List<Card> player1Cards = prepareCardLists("4H 4C 6S 7S KD");
        List<Card> player2Cards = prepareCardLists("2C 3S 9S 9D TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new Pair();

        List<Card> player1Cards = prepareCardLists("2H 2S 6S 7S KD");
        List<Card> player2Cards = prepareCardLists("2H 2S 9S 5D TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}