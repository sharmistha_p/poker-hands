package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class RoyalFlushTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new RoyalFlush();

        List<Card> player1Cards = prepareCardLists("AC JC KC QC TC");
        List<Card> player2Cards = prepareCardLists("2S 3S 4S 5S 6S");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new RoyalFlush();

        List<Card> player1Cards = prepareCardLists("AS JC KC QD TH");
        List<Card> player2Cards = prepareCardLists("AS QS JS TS KS");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new RoyalFlush();

        List<Card> player1Cards = prepareCardLists("AC JC KC QC TC");
        List<Card> player2Cards = prepareCardLists("AS QS JS TS KS");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}