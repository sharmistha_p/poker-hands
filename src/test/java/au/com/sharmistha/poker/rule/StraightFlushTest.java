package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class StraightFlushTest {
    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new StraightFlush();

        List<Card> player1Cards = prepareCardLists("8C 9C TC JC QC");
        List<Card> player2Cards = prepareCardLists("2C 3S 4S 5D 6D");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new StraightFlush();

        List<Card> player1Cards = prepareCardLists("3C 9C TC JC 2C");
        List<Card> player2Cards = prepareCardLists("2S 3S 4S 5S 6S");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new StraightFlush();

        List<Card> player1Cards = prepareCardLists("2S 3S 4S 5S 9S");
        List<Card> player2Cards = prepareCardLists("2S 3S 4S 5S 9S");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}