package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class StraightTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new Straight();

        List<Card> player1Cards = prepareCardLists("JH TC QS KS AD");
        List<Card> player2Cards = prepareCardLists("2C 9S 9S 9D TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new Straight();

        List<Card> player1Cards = prepareCardLists("TH 2H QS KS AD");
        List<Card> player2Cards = prepareCardLists("2C 3S 4S 5D 6D");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new Straight();

        List<Card> player1Cards = prepareCardLists("2C 3S 4S 5D 6D");
        List<Card> player2Cards = prepareCardLists("2C 3S 4S 5D 6D");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }

}