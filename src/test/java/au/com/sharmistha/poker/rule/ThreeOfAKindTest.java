package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class ThreeOfAKindTest {

    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new ThreeOfAKind();

        List<Card> player1Cards = prepareCardLists("AH AC AS 7S KD");
        List<Card> player2Cards = prepareCardLists("2C 3S 9S 9D TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new ThreeOfAKind();

        List<Card> player1Cards = prepareCardLists("4H AC 4S 7S KD");
        List<Card> player2Cards = prepareCardLists("2C 9S 9S 9D TD");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new ThreeOfAKind();

        List<Card> player1Cards = prepareCardLists("3H 5C 5S 5D KD");
        List<Card> player2Cards = prepareCardLists("2C TD 5C 5S 5D");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}