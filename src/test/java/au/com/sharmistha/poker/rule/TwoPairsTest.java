package au.com.sharmistha.poker.rule;

import au.com.sharmistha.poker.domain.Card;
import org.junit.Test;

import java.util.List;

import static au.com.sharmistha.poker.rule.RuleResult.*;
import static au.com.sharmistha.poker.utils.CardUtil.prepareCardLists;
import static org.assertj.core.api.Assertions.assertThat;

public class TwoPairsTest {
    @Test
    public void player1WinsTest(){
        CardChecker cardChecker = new TwoPairs();

        List<Card> player1Cards = prepareCardLists("AC AD KC QC QD");
        List<Card> player2Cards = prepareCardLists("2S 2D 4S 5S 6S");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER1_WON);
    }

    @Test
    public void player2WinsTest(){
        CardChecker cardChecker = new TwoPairs();

        List<Card> player1Cards = prepareCardLists("AC 5D KC 2C QD");
        List<Card> player2Cards = prepareCardLists("2S 2D 4S 4C AC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(PLAYER2_WON);
    }

    @Test
    public void tieTest(){
        CardChecker cardChecker = new TwoPairs();

        List<Card> player1Cards = prepareCardLists("AC AD KC QC QD");
        List<Card> player2Cards = prepareCardLists("2S 2D 4S 4C AC");

        RuleResult result = cardChecker.check(player1Cards, player2Cards);
        assertThat(result).isEqualTo(TIE);
    }
}